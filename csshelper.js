/**
 * Created by simh on 24.11.16.
 */

function isPrime(num) {
    if (num < 2) return false;
    if (num == 2) return true;
    if (num % 2 == 0) return false;
    for (var i = 3; i * i <= num; i += 2)
        if (num % i == 0) return false;
    return true;
}

function gcd(a, b) {
    if(b === 0)
        return a;
    return gcd(b, a % b);
}

function encrypt(message, pk) {
    if(!pk['value'] || !pk['N'])
        return;

    return powMod(message, pk.value, pk.N);
}

function decrypt(cipher, sk) {
    if(!sk['value'] || !sk['N'])
        return;

    return encrypt(cipher, sk);
}

function powMod(base, pow, mod) {
    // to be able to handle really large multiplications
    var buf = base % mod;
    for(var i = 2; i <= pow; i++) {
        buf *= (base % mod);
        buf %= mod;
    }

    return buf % mod;
}

// finds inverse for value in relation to modulo
function findMultInverse(value, modulo, itcnt) {
    var inverseExist = (gcd(value, modulo) == 1);
    if(!inverseExist)
        throw new Error("No inverse exist to " + value + " mod " + modulo + ".");

    itcnt = (itcnt > 1) ? itcnt : 10000;
    var result;
    for(var i = 1; i < itcnt ; i++) {
        result = (value * i) % modulo;
        if(result == 1)
            return i;
    }

    throw new Error("No inverse found. Try to increase 'itcnt' to more than " + itcnt);
}

function parity(bitstring) {
    var buf = 0;
    for(var i = 0; i < bitstring.length; i++) {
        buf ^= bitstring.charAt(i);
    }

    return buf;
}

module.exports = {
    isPrime: isPrime,
    gcd: gcd,
    ggT: gcd,
    rsa: {
        encrypt: encrypt,
        decrypt: decrypt
    },
    powMod: powMod,
    findMultInverse: findMultInverse,
    parity: parity
}